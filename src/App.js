import './App.css';
import AppNavbar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Error from './pages/Error';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import CourseView from './pages/CourseView';
function App() {
  const [user, setUser] = useState({id: null, isAdmin:false})

  const unSetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{
    fetch('/users/profile',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data)
      setUser({id: data._id, isAdmin: data.isAdmin})
    })
  }, [])
  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <BrowserRouter>
        <AppNavbar/>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/courses" element={<Courses/>}/>
          <Route path="/courses/:courseId" element={<CourseView/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="*" element={<Error/>}/>
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
