import React, { useEffect, useState, useContext } from 'react'
import { Row, Col, Button, Card, Container } from 'react-bootstrap'
import UserContext from '../UserContext'
import { Link } from 'react-router-dom'

const CourseCard = ({prop}) => {

  const  { _id, name, description, price, slots } = prop;

  // use the state hook for this component to be able to store its state specifically to monitor the number of employees
  // states are used to keep track information related to individual components
  // syntax:
    // const [getter, setter]=useState(initialGetterValue)

  const [enrollees, setEnrollees]=useState(0)
  const [slotsAvailable, setSlotsAvailable] = useState(slots)
  console.log(enrollees)

  const [isAvailable, setIsAvailable] = useState(true)
  const { user } = useContext(UserContext)
  // add useEffect hook to have the coursecard component do perform a certain task after every dom update
    // syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])
    useEffect(()=>{
      if(slotsAvailable===0){
        setIsAvailable(false)
      }
    },[slotsAvailable, enrollees])
    console.log(isAvailable)

  function enroll(){
    if(slotsAvailable>0){
      setEnrollees(enrollees+1)
      setSlotsAvailable(slotsAvailable-1)
    }else{
      document.getElementById(`btn${_id}`).disabled = true;
      alert('no more seats.')
    }
  }
  return (
    <Container>
    <Row>
        <Col xs={12} md={4} className="offset-md-4 offset-0">
            <Card>
                <Card.Body>
                    <Card.Title as="h3">{name}</Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>
                    {description}
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>
                    PHP {price}
                    </Card.Text>
                    <Card.Subtitle>Enrollees:</Card.Subtitle>
                    <Card.Text>
                    {enrollees}
                    </Card.Text>
                    <Card.Subtitle>Slots available:</Card.Subtitle>
                    <Card.Text>
                    {slotsAvailable} slots
                    </Card.Text>
                    {
                        user === null
                        ? <Button as={Link} to='/login' variant='primary' disabled={!isAvailable}>Details</Button>
                        : slotsAvailable === 0 ? (
                          <>
                            <p style={{color: "red", fontStyle: "italic"}} >No more slots</p>
                            <Button id={`btn${_id}`} variant="primary" onClick={enroll} disabled={isAvailable}>Details</Button>
                          </>
                        ) : (
                          <Button as={Link} to={`/courses/${_id}`} id={`btn${_id}`} variant="primary" onClick={enroll}>Details</Button>
                        )
                      }



                </Card.Body>
            </Card>
        </Col>
    </Row>
    </Container>
  )
}

export default CourseCard