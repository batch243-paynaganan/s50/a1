import React from 'react'
import { Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const NotFound = () => {
  return (
    <Row className='text-center'>
        <Col>
            <h1>404</h1>
            <h2>Nah bro, you in the wrong hood.</h2>
            <p>Get yo sorry ass outta here.</p>
            
            <Button as={Link} to='/' variant="primary">Return to Homepage</Button>
        </Col>
    </Row>
  )
}

export default NotFound