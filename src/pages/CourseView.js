import React, { useEffect, useState } from 'react'
import { Container, Card, Button, Row, Col } from 'react-bootstrap'
import { useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'

const CourseView = () => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState(0);

  const {courseId} = useParams();
  console.log(courseId)

  const history = useNavigate()
  useEffect(()=>{
    fetch(`/courses/${courseId}`)
    .then(res=>res.json())
    .then(data=>{
        console.log(data)
        setName(data.name)
        setDescription(data.description)
        setPrice(data.price)
    })
  },[courseId])

  const enroll = (courseId) => {
    fetch(`/users/enroll/${courseId}`,{
        method: 'POST',
        headers:{
            "Content-Type":"application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res=>res.json())
    .then(data=>{
        if(data){
            Swal.fire({
                title:'successfully enrolled',
                icon:'success',
                text:'sure ka na ba dyan, wala na backout-an dyan'
            })
            history('/courses')
        }else{
            Swal.fire({
                title:'something went wrong',
                icon:'error',
                text:'dasurv'
            })
            history('/')
        }
    })
  }
  return (
    <Container>
        <Row>
            <Col>
                <Card>
                    <Card.Body className='text-center'>
                    <Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						<Card.Subtitle>Class Schedule</Card.Subtitle>
						<Card.Text>8 am - 5 pm</Card.Text>
						<Button variant="primary" onClick={()=>enroll(courseId)}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
  )
}

export default CourseView