import React, { useEffect, useState } from 'react'
import CourseCard from '../components/CourseCard'
//import coursesData from '../data/courses'

const Courses = () => {
   const [courses, setCourses] = useState([])
   // console.log(coursesData)
   // const local =localStorage.getItem('email')
   useEffect(()=>{
      fetch('/courses/allActiveCourses')
      .then(res=>res.json())
      .then(data=>{
         console.log(data)
         setCourses(data.map(course=>{
            return(
               <CourseCard key={course._id} prop={course}/>
            )
         }))
      })
   },[])
   return(
    <>
    {courses}
    </>
   )
}

export default Courses