import React from 'react'
import { Container } from 'react-bootstrap'
import NotFound from '../components/NotFound'

const Error = () => {
  return (
    <Container>
        <NotFound/>
    </Container>
  )
}

export default Error