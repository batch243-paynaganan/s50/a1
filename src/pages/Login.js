import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { Container, Row, Col } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')  
  const [isActive, setIsActive] = useState(false)

  // const [user, setUser] = useState(localStorage.getItem('email'))
  // allows us to consume the userContext obj and its props to use for user validation
  const {user, setUser} = useContext(UserContext)

  useEffect(() => setIsActive(email && password), [email, password])

  const loginUser = e => {
    e.preventDefault()
    // alert(`Congratulations ${email} you are now logged in`)

    // localStorage.setItem('email',email)
    // setUser(localStorage.getItem('email'))

    // setEmail('')
    // setPassword('')
    fetch('/users/login',{
      method: 'POST',
      headers: {
        'Content-Type':'application/json'
      },
      body: JSON.stringify({
        email,
        password
      })
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data)
      if(data.accessToken !== 'empty'){
        localStorage.setItem('token', data.accessToken);
        retrieveUserDetails(data.accessToken);
        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: 'welcome to our website'
        })
      }else{
        Swal.fire({
          title: 'Authentication Failed',
          icon: 'error',
          text: 'Check your login details and try again.'
        })
        setPassword('')
      }
    })

    const retrieveUserDetails = (token) => {
      fetch('/users/profile',{
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
        setUser({id: data._id, isAdmin: data.isAdmin})
        console.log(user);
      })
    }
  }

  return (
    (user.id !== null)
    ? <Navigate to='/' /> 
    : <Container>
    <Row>
      <Col className='col-md-4 col-8 offset-md-4 offset-2'>
        <Form onSubmit={loginUser} className='bg-secondary p-3'>
          <Form.Group controlId='email'>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type='email'
              placeholder='Enter email'
              value={email}
              onChange={e => setEmail(e.target.value)}
              required
            />
            <Form.Text className='text-muted'>
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
          <Form.Group controlId='password1'>
            <Form.Label>Enter your Desired Password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Password'
              value={password}
              onChange={e => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <Button variant='primary' type='submit' disabled={!isActive}>
            Submit
          </Button>
        </Form>
      </Col>
    </Row>
    </Container>
  )
}

export default Login
