import React from 'react'
import { Navigate } from 'react-router-dom'
import { useContext, useEffect } from 'react'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

const Logout = () => {
  const { setUser, unSetUser } = useContext(UserContext)
  unSetUser()

  useEffect(()=>{
    setUser({id: null, isAdmin:false})
  },[setUser])

  Swal.fire({
    title: 'welcome back to the outside world',
    icon:'info',
    text:'babalik ka rin'
  })
  // once the logout is clicked it will rerender the appnavbar
  return (
    <Navigate to='/login'/>
  )
}

export default Logout
