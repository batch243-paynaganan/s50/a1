import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { Container, Row, Col } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import { Link, Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import { useNavigate } from 'react-router-dom'

const Register = () => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)

const { user, setUser } = useContext(UserContext)

useEffect(() => {
    if(firstName && lastName && mobileNo && email && password1 && password2 && password1 === password2 && mobileNo.length >= 11) {
    setIsActive(true)
    } else {
    setIsActive(false)
    }
}, [firstName, lastName, mobileNo, email, password1, password2])

const history = useNavigate()

function registerUser(event){
    event.preventDefault();

    
    fetch(`/users/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email:email, 
            password:password1, 
            firstName: firstName,
            lastName:lastName,
            mobileNo: mobileNo
        })
    })
    .then((res)=> {
        return res.json();
    })
    .then(data=>{
        console.log(data);
        if (data.error){
            Swal.fire({
                title:"Duplicate email",
                icon:'warning',
                text:data.error
            })
        }
        else{
            localStorage.setItem('email',email);
            setUser(localStorage.getItem('email'));
            setEmail('');
            setPassword1('');
            setPassword2('');
            setFirstName('');
            setLastName('');
            setMobileNo('');
            Swal.fire({
                title: "Registration Success",
                icon: "success",
                text:"Welcome! You are now registered to our website."
            })
            history('/login');
        }

    })
}

return (
    user.id !== null
    ? <Navigate to='/'/>
    : <Container>
        <Row>
            <Col className='col-md-4 col-8 offset-md-4 offset-2'>
                <Form onSubmit={registerUser} className='bg-secondary p-3'>
                <Form.Group className='mb-3' controlId='firstName'>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type='text' placeholder='Enter your first name' value={firstName} onChange={e=> setFirstName(e.target.value)} required/>
                </Form.Group>
                <Form.Group className='mb-3' controlId='lastName'>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type='text' placeholder='Enter your last name' value={lastName} onChange={e=> setLastName(e.target.value)} required/>
                </Form.Group>
                <Form.Group className='mb-3' controlId='mobileNo'>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control type='number' placeholder='Enter your mobile number' value={mobileNo} onChange={e=> setMobileNo(e.target.value)} required/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email}
                onChange={e => setEmail(e.target.value)} required/>
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Enter your Desired Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}required/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify your Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)}required/>
                </Form.Group>
                <Button as={Link} to='/login' variant="primary" type="submit" disabled={!isActive} onClick={registerUser}>
                Submit
                </Button>
                </Form>
            </Col>
        </Row>
    </Container>
)
}

export default Register